# Workshop 2: Getting Started with Python

This is the second in a series of scientific Python lectures at Risø campus,
Denmark Technical University.

## Workshop objective

To help Matlab-experienced users begin coding in Python. This includes a review
of how to import modules along with a brief introduction of the most commonly
used packages (NumPy, Matplotlib, and pandas).

## Who should come

This workshop is geared primarily towards new Python users that have previously
coded in Matlab. However, there are some topics that may be of use to more
experienced users, such as the package tutorials and PEP8 coding conventions.

## Date

The workshop date and location will be announced internally at DTU Risø. Please
use the contact information below for questions on the workshop contents or 
arranging a new workshop.

## Topic outline

- Import modules/packages  
- Useful packages:
    - NumPy/SciPy
    - Pandas
    - Matplotlib

## Prerequisites

If you are attending the workshop, please do the following before attending:
1. If you didn't attend Workshop 1, do those prerequisites ([link](https://gitlab.windenergy.dtu.dk/python-at-risoe/scientific-python-workshops/1-intro-to-conda#prerequisites)).
2. If you don't have git installed, [install it](https://git-scm.com/downloads)
with the following options:
    - Default git components
    - Either git bash only or git bash & Windows command prompt
    - Use OpenSSH
    - Open SSL
    - Checkout Windows-style, commit Unix-style
    - Use MinTTY
    - Default extra options
3. Clone the workshop repository to your computer.
    - In your git terminal (the application is called "Git bash" in Windows),
    change to a directory where you want the new folder to be placed.  E.g.,  
    `cd /c/Users/$USERNAME/Desktop`  
    Note that, in git bash, file paths are similar to Unix, with forward slashes
    and a different way to reference the C-drive. If you have a file path
    with a space in it, put a single quote at the beginning and end of the path.
    - Enter this command into the terminal:  
    `git clone https://gitlab.windenergy.dtu.dk/python-at-risoe/scientific-python-workshops/2-getting-started.git`
4. Test that git is set up properly by trying to pull.
    - Change into the newly cloned folder  
    `cd 2-getting-started/`
    - Enter this command:  
    `git pull origin master`
5. Just before the workshop, update the folder by pulling the changes (same
command as in Step 4).
6. Skim [python_vs_matlab.pdf](https://gitlab.windenergy.dtu.dk/python-at-risoe/scientific-python-workshops/2-getting-started/blob/master/python_vs_matlab.pdf)
and make sure you understand the basics of array creation and slicing.

## Contact

Jenni Rinker  
rink@dtu.dk